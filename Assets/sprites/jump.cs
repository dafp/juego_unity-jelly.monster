﻿using UnityEngine;
using System.Collections;

public class jump : MonoBehaviour {
	public float altura_salto;
	public float velocidad_movimiento;
	private Rigidbody2D rb;
	private bool tocar_piso;
	private Animator anim;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		anim.SetInteger ("Estado",1);
	}

	void OnCollisionEnter2D(Collision2D c)
	{
		tocar_piso = c.gameObject.tag.Equals("piso");
	}
	// Update is called once per frame
	void Update () {
		if (tocar_piso) 
		{
		anim.SetInteger ("Estado",0);
		}

		if (Input.GetKey (KeyCode.Space) && tocar_piso) 
		{
			rb.velocity = new Vector2 (rb.velocity.x*2, altura_salto*2);
			tocar_piso = false;
			anim.SetInteger("Estado",2);
	    }
		if (Input.GetKey (KeyCode.RightArrow))  
		{
			rb.velocity = new Vector2 (velocidad_movimiento, rb.velocity.y);
			rb.transform.localScale = new Vector2(1, 1);
			anim.SetInteger("Estado",1);
		}
		if (Input.GetKey (KeyCode.LeftArrow))  
		{
			rb.velocity = new Vector2 (-velocidad_movimiento, rb.velocity.y);
			rb.transform.localScale = new Vector2 (-1, 1);
			anim.SetInteger("Estado",1);
		}
	}

}
